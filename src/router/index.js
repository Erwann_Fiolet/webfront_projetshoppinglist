import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listing',
    name: 'Listing',
    component: () => import('../views/Listing.vue')
  },
  {
    path: '/new',
    name: 'ListForm',
    component: () => import('../views/ListForm.vue')
  },
  {
    path: '/list/:id',
    name: 'List',
    component: () => import('../views/List.vue')
  },
  {
    path: '/list/:id/add',
    name: 'ItemAdd',
    component: () => import('../views/ItemAdd.vue')
  },
  {
    path: '/map',
    name: 'Map',
    component: () => import('../views/FindStore.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
